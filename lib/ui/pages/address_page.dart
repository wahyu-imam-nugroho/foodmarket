part of 'pages.dart';

class AddressPage extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  @override
  Widget build(BuildContext context) {
    TextEditingController phoneController = TextEditingController();
    TextEditingController addresController = TextEditingController();
    TextEditingController houseNumController = TextEditingController();

    return GeneralPage(
      title: 'Address',
      subtitle: "Make sure it's valid",
      onBackButtonPressed: () {
        Get.back();
      },
      child: Column(
        children: [
          Container(
            width: 110,
            margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
            child: Text(
              "Phone Number",
            style: blackFontStyle2,
          ),
    ),
    Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: defaultMargin),
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.black)),
      child: TextField(
        controller: phoneController,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintStyle: greyFontStyle,
          hintText: 'Type your phone number'),
        ), 
      ),
    Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
      child: Text(
        "email Addrres",
        style: blackFontStyle2,
        ), 
      ),
      Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 24),
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: Colors.black)),
        child: TextField(
          controller: addresController,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: greyFontStyle,
            hintText: 'Type your addres'),
        ),
      ),
      Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
        child: Text(
          "House number",
          style: blackFontStyle2,
        ),
      ),
      Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: defaultMargin),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: Colors.black)),
        child: TextField(
          controller: houseNumController,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintStyle: greyFontStyle,
            hintText: "Type your house number"),
        ),
      ),
        Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
      child: Text(
        "City",
        style: blackFontStyle2,
      ),
        ),
        Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 24),
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: Colors.black)),
        child: DropdownButton(
          isExpanded: true,
          underline: SizedBox(),
          items: [
          DropdownMenuItem(
            child: Text(
            'Jepara',
             style: blackFontStyle3,
          )),
          DropdownMenuItem(
            child: Text(
            'Kudus',
             style: blackFontStyle3,
          )),
          DropdownMenuItem(
            child: Text(
            'Semarang',
             style: blackFontStyle3,
          )),
        ], onChanged: (item) {} ),
        ),
        Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 24),
        height: 45,
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: RaisedButton(
          onPressed: () {},
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)),
            color: mainColor,
        child: Text(
          'Continue',
          style: GoogleFonts.poppins(
            color: Colors.black, fontWeight: FontWeight.w500),
          ),
        ),
        ),
    ],
    ),
    );
  }
}